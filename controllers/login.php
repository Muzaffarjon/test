<?php
session_start();
class Controller_Login Extends Controller_Base {

    // шаблон
    public $layouts = "login_layouts";

    // экшен
    function index() {

        $alogin=new Model_Login();

        if(!isset($_SESSION['user'])){

        if(isset($_POST['ok'])){
            if(isset($_POST['login']) && isset($_POST['password'])){
                if(!empty($_POST['login']) && !empty($_POST['password'])){
                    $login=$_POST['login'];$password=$_POST['password'];
                    $test=$alogin->authLogin($login,$password);

                    if(!empty($test)){
                        $this->template->vars('errorMessage', 'вы успешно прошли авторизацию');
                        $alogin->sessions();
                        $alogin->redirect('index');
                    }else
                        $this->template->vars('errorMessage', 'Не верный логин или пароль');
                }else $this->template->vars('errorMessage', 'Заполните поля пожалуйста');
            }else $this->template->vars('errorMessage', 'Одно из полей не заполнено');
        }

        $this->template->view('login');
    }else{
            ob_start();
            echo 'output';
            header("Location: /index");
            ob_end_flush();
        }

    }

}

?>