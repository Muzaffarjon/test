<?php
session_start();

// контролер
Class Controller_Index Extends Controller_Base
{
    public $start_date='';
    public $end_date='';

    // шаблон
    public $layouts = "first_layouts";

    function __construct($registry)
    {
        $this->start_date=date('d/m/Y', strtotime('1 months ago'));
        $this->end_date=date('d/m/Y');

        parent::__construct($registry);
    }

    // экшен
    function index()
    {
        if (isset($_SESSION['user'])) {
            $dates=$this->getDates('2020-03-02','2020-04-02');
            $this->template->vars('dates', $dates);
            $this->template->view('index');

        } else {

            $this->logout();
        }
    }

    function getAjax(){
        if(isset($_GET['date'])){
            header('Content-type: application/json');
            $model=new Model_Currency();
            $data=$model->getCurrencyByDate($_GET['date']);
            echo json_encode($data);
        }
    }

    function currency(){
        $valuteID=$_GET['valuteID'];
        $from=date('d/m/Y',strtotime($_GET['from']));
        $to=date('d/m/Y',strtotime($_GET['to']));

        $model=new Model_Currency();
        $data=$model->getCurrencyByValuteId($valuteID,$from,$to);
        echo json_encode($data);
    }

    function get_currency()
    {

        $days = $this->getDates(date('Y-m-d', strtotime('1 months ago')), date('Y-m-d'));

        foreach ($days as $day) {
           echo $this->insertToDatabase($day);
        }
    }

    function currency_dynamic(){

       echo $this->getDynamicCurrency();

    }

    /**
     * @param $startTime
     * @param $endTime
     * @return array
     */
    function getDates($startTime, $endTime)
    {
        $day = 86400;
        $format = 'd/m/Y';
        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime);
        $numDays = round(($endTime - $startTime) / $day); // без +1

        $days = array();

        for ($i = 0; $i <= $numDays; $i++) {
            $days[] = date($format, ($startTime + ($i * $day)));
        }

        return $days;
    }

    function insertToDatabase($date)
    {
        $url = "http://www.cbr.ru/scripts/XML_daily.asp?date_req=$date";
        $data = simplexml_load_file($url);
        $json = json_encode($data);
        $array = json_decode($json, TRUE);
        array_splice($array, 0, 1);
        foreach ($array as $arr) {
            foreach ($arr as $key) {
                $model_currency = new Model_Currency();
                // добавления в базу данных
                $model_currency->addCurrency($date, $key['@attributes']['ID'], $key['NumCode'], $key['CharCode'], $key['Nominal'], $key['Name'], $key['Value']);
            }
        }
    }

    function insertCurrencyWithDynamic($xml){
        foreach ($xml as $item) {
            foreach ($item as $key) {

                $model_currency=new Model_Currency();

                $model_currency->addCurrencyDynamic(date('d/m/Y',strtotime($key['@attributes']['Date'])),$key['@attributes']['Id'],$key['Nominal'],$key['Value']);
            }

        }
    }

    /**
     * @param $currencyId
     */
    function getDynamicCurrency(){

        $model=new Model_Currency();

        $currency_group=$model->getCurrencyGroup();

        foreach ($currency_group as $vl){

             $xml=$this->getRequestXmlDynamic($vl['valuteID']);

             $this->insertCurrencyWithDynamic($xml);
        }
    }

    /**
     * @param $currencyId
     * @return mixed
     */
    function getRequestXmlDynamic($currencyId){

        $url = "http://www.cbr.ru/scripts/XML_dynamic.asp?date_req1=$this->start_date&date_req2=$this->end_date&VAL_NM_RQ=$currencyId";

        $data = simplexml_load_file($url);
        $json = json_encode($data);
        $array = json_decode($json, TRUE);
        array_splice($array, 0, 1);
        return $array;
    }

    // Выход из системы
    function logout()
    {
        $login = new Model_Login();
        $login->logout();
        $login->redirect('login');
    }

    // Перенаправления
    function redirect($url)
    {
        $login = new Model_Login();
        $login->redirect($url);
    }

}