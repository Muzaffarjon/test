
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.8.3/jquery.min.js" integrity="sha256-YcbK69I5IXQftf/mYD8WY0/KmEDCv1asggHpJk1trM8=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {

        $('.setdate').on('change', function() {
        $.getJSON('/getAjax?date=' + this.value,

            function (json) {
                $('table tr td').remove();
                console.log(json);
                var tr;
                for (var i = 1; i < json.length; i++) {
                    tr = $('<tr/>');
                    tr.append("<td>" + i + "</td>");
                    tr.append("<td>" + json[i].valuteID + "</td>");
                    tr.append("<td>" + json[i].numCode + "</td>");
                    tr.append("<td>" + json[i].сharCode + "</td>");
                    tr.append("<td>" + json[i].nominal + "</td>");
                    tr.append("<td>" + json[i].name + "</td>");
                    tr.append("<td>" + json[i].value + "</td>");
                    $('table').append(tr);
                }
            });
        });
    });
</script>

<form style="margin-top: 20px;">
    <select class="setdate">
        <?php foreach ($dates as $date) {
            echo "<option value='$date'>$date</option>";
        } ?>

    </select>
</form>

<table id="table" style="margin-top: 20px;" class="table">

    <thead class="thead-dark">

        <th scope="col">#</th>
        <th scope="col">ValuteId</th>
        <th scope="col">NumCode</th>
        <th scope="col">CharCode</th>
        <th scope="col">Nominal</th>
        <th scope="col">Name</th>
        <th scope="col">Value</th>

    </thead>
    <tbody>
    </tbody>
</table>