<?php
include("classes/DBController.php");

// модель
Class Model_Currency
{

    private $db_handle;

    private $date;

    /**
     * Model_Tasks constructor.
     */
    function __construct()
    {
        $this->db_handle = new DBController();
        $this->date = time();
    }

    /**
     * @param $date
     * @param $valuteID
     * @param $numCode
     * @param $сharCode
     * @param $nominal
     * @param $name
     * @param $value
     * @return int
     */
    function addCurrency($date, $valuteID, $numCode, $сharCode, $nominal, $name, $value)
    {

        $query = "INSERT INTO currency (date,valuteID,numCode,сharCode,nominal,name,value,created_at,status) VALUES (?, ?, ?,?,?,?,?,?,?)";
        $paramType = "sssssssss";
        $paramValue = array(
            $date,
            $valuteID,
            $numCode,
            $сharCode,
            $nominal,
            $name,
            $value,
            $this->date,
            1
        );
        $this->db_handle->insert($query, $paramType, $paramValue);
        return false;
    }

    function addCurrencyDynamic($date, $valuteID, $nominal, $value)
    {
        $query = "INSERT INTO dynamic_currency (date,valuteID,nominal,value,created_at,status) VALUES (?, ?, ?,?,?,?)";
        $paramType = "ssssss";
        $paramValue = array(
            $date,
            $valuteID,
            $nominal,
            $value,
            $this->date,
            1
        );
        $this->db_handle->insert($query, $paramType, $paramValue);
        return false;
    }

    function getCurrencyGroup(){
        $query = "SELECT * FROM currency GROUP BY valuteID";
        $result = $this->db_handle->runBaseQuery($query);
        return $result;
    }

    /**
     * @param $valuteId
     * @return array
     */
    function getCurrencyByValuteId($valuteId,$from,$to)
    {
        //выборка по id записи
        $query = "SELECT * FROM currency WHERE valuteID = '$valuteId' AND `date` BETWEEN '$from' AND '$to'";
        $result = $this->db_handle->runBaseQuery($query);
        return $result;
    }

    /**
     * @param $date
     * @return array
     */
    function getCurrencyByDate($date)
    {
        $sql = "SELECT * FROM currency WHERE date='$date'";
        $result = $this->db_handle->runBaseQuery($sql);

        return $result;
    }

}