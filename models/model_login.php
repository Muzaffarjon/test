<?php
include("classes/DBController.php");
// модель
Class Model_Login{

    private $db_handle;

    private $users;
    function __construct() {
        $this->db_handle = new DBController();
    }

    /**
     * @param $login
     * @param $password
     * @return array|int
     */
    function authLogin($login, $password) {
        $sql="SELECT * FROM users WHERE login = '".$login."' AND password = '".$password."' LIMIT 1";
        $this->users = $this->db_handle->runBaseQuery($sql);
        return empty($this->users) ? 0 : $this->users;
    }

    function sessions(){
        session_start();
        $_SESSION['user']=$this->users;
    }

    function logout() {
        session_destroy();
    }

    /**
     * @param $route
     */
    function redirect($route){
        ob_start();
        echo 'output';
        header("Location: /$route");
        ob_end_flush();
    }
}